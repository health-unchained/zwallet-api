"use strict";

//configuration
const APP_PORT = process.env['APP_PORT'] || 3000;
const APP_SECRET = process.env['APP_SECRET'] || 'SecretZwalletIsVerySecret';

//express + middleware
const express = require('express');
const expressJwt = require('express-jwt');
const jwt = require('jsonwebtoken');
const app = express();

//parse application/json
app.use(express.json());

//use jwt for authentication / authorization
//permissions possible via https://github.com/MichielDeMey/express-jwt-permissions
app.use(expressJwt({secret: APP_SECRET, credentialsRequired: false}));

app.use(function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    next();
});

app.post('/login', function (req, res) {
    //read credentials from json body
    let credentials = req.body['credentials'] || {};
    //arbitrary check on credentials
    if (credentials['email'] === 'info@zwallet.org' && credentials['password'] === 'Zwallet1337') {
        //sign jwt token
        let token = jwt.sign({'e': 'info@zwallet.org'}, APP_SECRET);
        res.send(JSON.stringify({'token': token}));
    } else {
        res.status(401).send();
    }
});

app.get('/user', function (req, res) {
    //if token is supplied in header, user property will be present with token claims
    //Authorization: Bearer {token} (example: Authorization: Bearer Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlIjoiaW5mb0B6d2FsbGV0Lm9yZyIsImlhdCI6MTUyMjcwMTg1Mn0.D0fB8KIbNSBRvoBY6Kavf1zfVOD5yAGixoonSbxdSvs)
    res.send(JSON.stringify(req.user));
});

app.listen(APP_PORT, function () {
    console.log('Listening on port', APP_PORT);
});